/**
 * @file neobotix_mpo700.h
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Header file for Neobotix MPO700 in RKCL
 * @date 12-03-2020
 * License: CeCILL
 */
#include <rkcl/drivers/neobotix_mpo700_driver.h>
#include <rkcl/processors/neobotix_mpo700_command_adapter.h>
