/**
 * @file neobotix_mpo700_command_adapter.h
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Utility class used to adapt the mobile base command and make it more appropriate to reactive control
 * @date 12-03-2020
 * License: CeCILL
 */
#pragma once

#include <rkcl/data/robot.h>
#include <rkcl/data/crtp.h>

namespace rkcl {


class NeobotixMPO700CommandAdapter : public Callable<NeobotixMPO700CommandAdapter> {
public:

	NeobotixMPO700CommandAdapter(JointGroupPtr joint_group,
	                             ControlPointPtr control_point);

	NeobotixMPO700CommandAdapter(
	    Robot& robot,
	    const YAML::Node& configuration);

	~NeobotixMPO700CommandAdapter() = default;

	bool process();

	void init();


private:
	double prepositioning_velocity_;
	double threshold_error_task_enable_, threshold_error_task_disable_;
	JointGroupPtr joint_group_;
	ControlPointPtr control_point_;
	bool is_threshold_error_task_enabled_;

	Eigen::VectorXd last_velocity_prep_;
};

}
